# SSL Expiry Monitor Bot for Hangouts Chat

A super-basic webhook bot for Hangouts Chat that monitors the SSL certs for a
a set of sites. It is built in AppEngine Standard w/ PHP, and leverages built-in
cron functionality for scheduling

### Included files
app.yaml - AppEngine configuration file (include configs for sites and chat room)

cron.yaml - Cron schedule for AppEngine

chatbot.php - Main app. Can customized notification schedule in here

php.ini - Custom PHP module config for loading curl.so


### Setup
The app runs on AppEngine Standard on Google Cloud Platform. You will need a Google Cloud Platform account & gcloud CLI tools to deploy this code.