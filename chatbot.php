<?php

#Get the hosts to check from app.yaml
$sites_csv = getenv('SITES');
$sites_arr = explode(",", $sites_csv);
$hc_url = getenv('HANGOUTS_CHAT_ROOM');

foreach ($sites_arr as $site)
{

	$url = "https://" . $site;
	$orignal_parse = parse_url($url, PHP_URL_HOST);
	$get = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
	$read = stream_socket_client("ssl://".$orignal_parse.":443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $get);
	$cert = stream_context_get_params($read);
	$certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
	$valid_to = date(DATE_RFC2822,$certinfo['validTo_time_t']);
	syslog(LOG_DEBUG, "[$site] - Valid to: $valid_to");
	$now = time();
	//Convert seconds into days.
	$difference = $certinfo['validTo_time_t'] - $now;
	$days = floor($difference / (60*60*24) );
	
	//Logic here to send alerts at 30 days, 10 days, and then every day after 5 days until expiration
	if ($days == 30 || $days == 10 || $days < 6)
	{
		sendHangoutsChat($site, $days, $hc_url);
	}
}



function sendHangoutsChat($site, $days, $url)
{
	//Format message appropriately
	$message = "SSL cert will expire in" . $days . " days";
	if ($days == 1) {$message = "SSL cert will expire in " . $days . " day";}
	if ($days == -1) {$message = "[WARNING] SSL cert expired " . abs($days) . " day ago";}
	if ($days < -1) {$message = "[WARNING] SSL cert expired " . abs($days) . " days ago";}
	
		
	//Build message content based upon msgType
	$contentToSend = "<b>" . $site . "</b><br>" . $message;
	$notifyContent = "";

	//Initiate cURL.
	$ch = curl_init($url);
 
	//The JSON data.
	$jsonData = array(
		"sender" => array(
        	"displayName" => "" . "SSL Expiry",
        	),
     	"cards" => array(
     		"sections" => array(
     			"widgets" => array(
     				"textParagraph" => array("text" => $contentToSend),
     				),
 	 			),
 	 	),
 	 	"fallbackText" => $notifyContent,
 	 );
 	 

	//Encode the array into JSON.
	$jsonDataEncoded = json_encode($jsonData);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);
}